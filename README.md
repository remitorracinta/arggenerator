# README #

This tool generates commands for a random hyper-parameter search using a simple config file.
Each command is composed of arguments whose value is generated randomly. These values
can be generated from a fixed number of possible values or from a range.


### How do I get set up? ###

Clone the repository and run
```mvn package```
which will generate a jar in the target folder.

### How do I run the tool ###

The tool takes three arguments:
```--config path/to/config --output path/to/output --num-commands {integer}```
Take a look at the example config file to see what that looks like.
All arguments are configured with the following syntax and seperated with an empty line:
```
argname
[categorical|uniform|log-uniform|int]
option1
option2
option3
...

argname2
...
```
- categorical will choose a value from each option with equal probability.
- uniform will pick a float between option1 and option2 uniformly
- log-uniform will generate a value by exponentiatiating a float between log(option1) and log(option2)
- int will pick an int between option1 and option2.
For all argtypes besides categorical, options besides option1 and option2 will be ignored.